import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailSubPage } from './detail-sub.page';

const routes: Routes = [
  {
    path: '',
    component: DetailSubPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailSubPageRoutingModule {}
