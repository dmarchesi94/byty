import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DetailMarkerService } from '../detail-marker.service';

@Component({
  selector: 'app-detail-sub',
  templateUrl: './detail-sub.page.html',
  styleUrls: ['./detail-sub.page.scss'],
})
export class DetailSubPage implements OnInit {
  constructor(
    private activatedRoute: ActivatedRoute,
    private detailService: DetailMarkerService
  ) {}
  detail: any;
  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      if (!paramMap.has('detailId')) {
        // redirect
        return;
      }
      // il più converte in number
      const detailId = +paramMap.get('detailId');
      this.detail = this.detailService.getMarker(detailId);
    });
  }
}
