import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DetailMarkerService {
  private data = {
    elements: [
      {
        type: 'node',
        id: 264145740,
        lat: 44.3481764,
        lon: 11.7156244,
        tags: {
          amenity: 'drinking_water',
          created_by: 'JOSM',
        },
      },
      {
        type: 'node',
        id: 884222702,
        lat: 44.355323,
        lon: 11.713047,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 884222883,
        lat: 44.3504623,
        lon: 11.7173583,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 973558253,
        lat: 44.3481246,
        lon: 11.717317,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 973558256,
        lat: 44.3483511,
        lon: 11.7182527,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 989180571,
        lat: 44.35306,
        lon: 11.7234121,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 1089326567,
        lat: 44.3577641,
        lon: 11.7028519,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 1311768629,
        lat: 44.3513772,
        lon: 11.7158453,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 1311768638,
        lat: 44.3510976,
        lon: 11.7033246,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 1321798250,
        lat: 44.364295,
        lon: 11.7014475,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 1321798286,
        lat: 44.3529223,
        lon: 11.7206476,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 1331017202,
        lat: 44.3536239,
        lon: 11.6970247,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 2266320546,
        lat: 44.3560311,
        lon: 11.7146549,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 2321173689,
        lat: 44.3483269,
        lon: 11.70954,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 2435635113,
        lat: 44.3650048,
        lon: 11.6904143,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 2494108908,
        lat: 44.3487084,
        lon: 11.7075976,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 2499745098,
        lat: 44.3497007,
        lon: 11.7080763,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 2601654819,
        lat: 44.3599175,
        lon: 11.7049117,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 2873218101,
        lat: 44.3564655,
        lon: 11.7089608,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 3613292893,
        lat: 44.3498578,
        lon: 11.6905747,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 3674977779,
        lat: 44.3546801,
        lon: 11.7079656,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 4316973807,
        lat: 44.3501962,
        lon: 11.7058747,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 4316973808,
        lat: 44.3501886,
        lon: 11.7063706,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 4577384045,
        lat: 44.3511457,
        lon: 11.698668,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 5082242876,
        lat: 44.3683188,
        lon: 11.6989128,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 5116063356,
        lat: 44.3618927,
        lon: 11.7096775,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 5169108845,
        lat: 44.3556672,
        lon: 11.7161341,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 5552751722,
        lat: 44.3531123,
        lon: 11.7136956,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 5568340329,
        lat: 44.3604395,
        lon: 11.7214572,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 5619946447,
        lat: 44.3541739,
        lon: 11.7016871,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 5620571663,
        lat: 44.3553111,
        lon: 11.6947032,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 5652563952,
        lat: 44.3552947,
        lon: 11.7130143,
        tags: {
          amenity: 'drinking_water',
        },
      },
      {
        type: 'node',
        id: 5692085375,
        lat: 44.3628918,
        lon: 11.6971206,
        tags: {
          amenity: 'drinking_water',
        },
      },
    ],
  };
  constructor() {}
  getAllMarker() {
    return this.data.elements;
  }
  getMarker(detailId: number) {
    return this.data.elements.find(x => x.id === detailId);
  }
}
